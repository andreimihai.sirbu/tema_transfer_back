const sequelize = require("./config");

module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "masini",
    {
      marca: DataTypes.STRING,
      cp: DataTypes.INTEGER,
      culoare: DataTypes.STRING,
      capacitateCilindrica: DataTypes.FLOAT,
      pret: DataTypes.FLOAT,
      anFabricatie: DataTypes.INTEGER,
      impozit: DataTypes.INTEGER,
      asigurare: DataTypes.FLOAT,
      email: DataTypes.STRING,
      telefon: DataTypes.STRING,
    },
    {
      underscored: true,
      tableName: "masini",
    }
  );
};
