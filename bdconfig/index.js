const db = require("./config");
const Masina = db.import("./masinuta");
module.exports = {
  connection: db,
  Masina,
};
