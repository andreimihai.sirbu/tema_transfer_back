const express = require("express");
const bodyParser = require("body-parser");
const { Masina } = require("./bdconfig");
// const { where } = require("sequelize/types");
// const { like } = require("sequelize/types/lib/operators");
const server = express();
server.use(bodyParser.json());
const PORT = 8080;

server.listen(PORT, () => {
  console.log("Server-ul ruleaze pe: " + PORT);
});

const connection = require("./bdconfig").connection;
//Ruta pentru resetarea bazei de date
server.get("/reset", async (req, res) => {
  connection
    .sync({
      force: true,
    })
    .then(() => {
      res.status(200).send({
        message: "Baza de date resetata!",
      });
    })
    .catch(() => {
      res.status(500).send({
        message: "Server error!",
      });
    });
});

//De implementat rutele din cerinta

//POST
//#region POST
server.post("/api/register", async (req, res) => {
  const errors = [];

  const masinuta = {
    marca: req.body.marca,
    cp: req.body.cp,
    culoare: req.body.culoare,
    capacitateCilindrica: req.body.capacitateCilindrica,
    pret: req.body.pret,
    anFabricatie: req.body.anFabricatie,
    //impozit: req.body.impozit,
    //asigurare: req.body.asigurare,
    email: req.body.email,
    telefon: req.body.telefon
  }

  if (!masinuta.marca || !masinuta.cp || !masinuta.culoare || !masinuta.capacitateCilindrica || !masinuta.pret || !masinuta.anFabricatie || !masinuta.email || !masinuta.telefon) {
    errors.push("Nu ai completat toate campurile!\n")
  }

  if (!/^[a-zA-Z]+$/.test(masinuta.marca)) {
    errors.push("Marca invalida!")
  }

  if (masinuta.cp <= 50) {
    errors.push("Ia-ti ceva care sa traga, cioroiule!")
  }

  if (!/^[a-zA-Z]+$/.test(masinuta.culoare)) {
    errors.push("Culoare invalida")
  }

  if (!/^[0-9]{1}\.[0-9]{1}$/.test(masinuta.capacitateCilindrica)) {
    errors.push("Capacitate cilindrica invalida!")
  }

  if (masinuta.pret <= 0 || !/^[1-9]{1}[0-9]+$/.test(masinuta.pret)) {
    errors.push("Usor, ca doar n-o iei de pomana! (Pret invalid)")
  }

  if (masinuta.anFabricatie <= 1900) {
    errors.push("Ce-i cu cazanu asta Johnule?")
  }

  else if (masinuta.anFabricatie > new Date().getFullYear()) {
    errors.push("Masina viitorului e inca in viitor!")
  }

  if (!/[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/.test(masinuta.email)) {
    errors.push("Email invalid!")
  }

  if (!/^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/.test(masinuta.telefon)) {
    errors.push("Formatul numarului de telefon este invalid!")
  }

  if (errors.length === 0) {
    try {
      masinuta.impozit = 100 + (masinuta.capacitateCilindrica - 1.00) / 0.2 * 50
      masinuta.asigurare = 0.07 * masinuta.pret
      await Masina.create(masinuta)
      res.status(201).send({
        message: 'Masina adaugata!'
      })
    } catch (error) {
      console.log(error);
      res.status(500).send({
        message: 'Eroare la crearea unei masini!'
      })
    }
  } else {
    res.status(400).send({
      errors
    })
  }
});
//#endregion POST


//PUT
//#region PUT
server.put("/api/update/:id", async (req, res) => {
  const errors = [];

  try {
    const masinuta = await Masina.findOne({
      where: {
        id: req.params.id
      }
    })

    if (masinuta) {
      ok = 1
      //validarile de la POST
      if (!masinuta.marca || !masinuta.cp || !masinuta.culoare || !masinuta.capacitateCilindrica || !masinuta.pret || !masinuta.anFabricatie || !masinuta.email || !masinuta.telefon) {
        errors.push("Nu ai completat toate campurile!\n")
        ok = 0
      }

      if (!/^[a-zA-Z]+$/.test(masinuta.marca)) {
        errors.push("Marca invalida!")
        ok = 0
      }

      if (masinuta.cp <= 50) {
        errors.push("Ia-ti ceva care sa traga, cioroiule!")
        ok = 0
      }

      if (!/^[a-zA-Z]+$/.test(masinuta.culoare)) {
        errors.push("Culoare invalida")
        ok = 0
      }

      if (!/^[0-9]{1}\.[0-9]{1}$/.test(masinuta.capacitateCilindrica)) {
        errors.push("Capacitate cilindrica invalida!")
        ok = 0
      }

      if (masinuta.pret <= 0 || !/^[1-9]{1}[0-9]+$/.test(masinuta.pret)) {
        errors.push("Usor, ca doar n-o iei de pomana! (Pret invalid)")
        ok = 0
      }

      if (masinuta.anFabricatie <= 1900) {
        errors.push("Ce-i cu cazanu asta Johnule?")
        ok = 0
      }

      else if (masinuta.anFabricatie > new Date().getFullYear()) {
        errors.push("Masina viitorului e inca in viitor!")
        ok = 0
      }

      if (!/[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/.test(masinuta.email)) {
        errors.push("Email invalid!")
        ok = 0
      }

      if (!/^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/.test(masinuta.telefon)) {
        errors.push("Formatul numarului de telefon este invalid!")
        ok = 0
      }

      if (ok) {
        let impozitNou = 100 + (req.body.capacitateCilindrica - 1.0) / 0.2 * 50
        //impozitNou = impozitNou *
        let asigurareNoua = req.body.pret * 0.07
        //asigurareNoua = asigurareNoua * 0.07 
        masinuta.update({
          marca: req.body.marca,
          cp: req.body.cp,
          culoare: req.body.culoare,
          capacitateCilindrica: req.body.capacitateCilindrica,
          pret: req.body.pret,
          anFabricatie: req.body.anFabricatie,
          email: req.body.email,
          telefon: req.body.telefon,
          impozit: impozitNou,
          asigurare: asigurareNoua

        })
          .then(() => {
            // masinuta.impozit = 100 + (masinuta.capacitateCilindrica - 1.00) / 0.2 * 50
            // masinuta.asigurare = 0.07 * masinuta.pret
            res.status(200).send({
              message: "Masina updatata"
            })
          })
      }
      else {
        res.status(400).send({
          errors
        })
      }
    }

  } catch {
    res.status(500).send({
      message: "Eroare la update"
    })
  }
})
//module.exports = router;

//#endregion PUT

//GET
//#region GET
server.get("/", (req, res) => {
  res.status(200).send({
    message: "Hello World!"
  });
});

server.get("/api/masini", async (req, res) => {
  Masina.findAll({
    attributes: ['id', 'marca', 'pret', 'cp']
  })
    .then(masini => {
      res.status(200).send(masini)
    })
    .catch(() => {
      res.status(500).send({
        message: "Eroare BD"
      })
    })
})

server.get("/api/masini/:marca", async (req, res) => {
  Masina.findAll({
    attributes: ['id', 'marca', 'pret', 'cp'],
    where: {
      marca: req.params.marca
    }
  })
    .then(masini => {
      res.status(200).send(masini)
    })
    .catch(() => {
      res.status(500).send({
        message: "Eroare BD"
      })
    })
})

//#endregion GET

//DELETE
server.delete(`/api/remove/:anFabricatie`, async (req, res) => {
  connection.query(`DELETE FROM masini WHERE masini.an_fabricatie < ${req.params.anFabricatie}`,
        function(err, result, fields) {
            if (err) {
                console.log(err);
            } else {
                console.log("deleted Record: " + result.affectedRows);
                
            }
        });
});

